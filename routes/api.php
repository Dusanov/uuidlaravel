<?php

Route::namespace('Auth')->group(function() {

	Route::post('register', 'RegisterController');

	Route::post('auth/login', 'LoginController');

	Route::post('logout', 'LogoutController');
});


Route::namespace('Profile')->middleware('auth:api')->group(function() {

	Route::post('create-new-profile', 'ProfileController@store');

	Route::patch('update-profile/{profile}', 'ProfileController@update');

	
});

Route::get('profiles/{profile}', 'Profile\ProfileController@show');

Route::get('user', 'UserController');

Route::group([
	'middleware' => 'api',
	'prefix'	 => 'campaign',
],function(){
  	Route::get('random/{count}', 'CampaignController@random' );
  	Route::post('store', 'CampaignController@store');
  	Route::get('/', 'CampaignController@index');
  	Route::get('/{id}', 'CampaignController@detail');
  	Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
	'middleware' => 'api',
	'prefix'	 => 'blog',
],function(){
  	Route::get('random/{count}', 'BlogController@random' );
  	Route::post('store', 'BlogController@store');
});
