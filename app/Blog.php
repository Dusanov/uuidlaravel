<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidTrait;

class Blog extends Model
{
    use UuidTrait;

    protected $guarded = [];
}
