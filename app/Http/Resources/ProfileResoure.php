<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResoure extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'published' => $this->created_at->format("d F Y"),
            'subject' => $this->subject->name,
            'author' => $this->user->name,
        ];
    }

    //public function with($request)
    //{
        //return ['status' => 'success'];
    //}
}
