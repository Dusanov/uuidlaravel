<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidTrait;

class Campaign extends Model
{
    use UuidTrait;

    protected $guarded = [];
}
