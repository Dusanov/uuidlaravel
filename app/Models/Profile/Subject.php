<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
