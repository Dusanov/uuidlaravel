<?php

namespace App\Models\Profile;

use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidTrait;

class Profile extends Model
{
	//use UuidTrait;

	//protected $guarded = [];


    protected $fillable = ['name', 'slug', 'address', 'profile_picture', 'subject_id'];

    protected $with = ['subject', 'user'];

    
    public function getRouteKeyName()
    {
    	return 'slug';
    }

    
    public function user()

    {

    	return $this->belongsTo(User::class);

    }

    public function subject()

    {

    	return $this->belongsTo(Subject::class);

    }

    
}
