<?php

/*namespace App\Profile;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = ['user_id', 'valid_until', 'codeotp'];

    protected $with = ['subject', 'user'];

    public function otp()
    {
    	return 'codeotp';
    }
}
