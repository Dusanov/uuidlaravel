<?php

namespace App\Models;

use App\Models\Profile\Profile;
use App\Models\Profile\Otp;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
//use App\Traits\UuidTrait;
//use App\Traits\OtpCode;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
        

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        if($this->rolle_id==1) {
            return true;
        }
        return false;
    }

    /*public static function boot()
    {

        parent::boot();
        static::creating(function($model){
            $model->otp = $model->otp();
        });

        static::created(function($model){
            $model->gererate_otp_code();
        });

    }*/
    
    public function generate_otp_code()
    {
        do {
    
            $random = mt_rand(100000,999999);
            $check  = OtpCode::where('otp' , $random)->first();
       }while($check);

       $now = Carbon::now();

       $otp_code = OtpCode::updateOrCreate (
        ['user_id' => $this->id],
        ['otp'  => $random, 'valid_until' => $now->addMinutes(5)]
       
       );
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}

