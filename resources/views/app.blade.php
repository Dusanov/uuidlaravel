<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="{{url('/css/app.css')}}">
</head>
<body>
	<h1>Sanbercode</h1>

	<div id="app">
		<app></app>
	</div>
	<script src="{{url('/js/app.js')}}"></script>
</body>
</html>